[![pipeline status](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/c-programming/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/c-programming/-/commits/main)

# C Programming

* HTML Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/c-programming/C-programming.html
* PDF Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/c-programming/C-programming.pdf
